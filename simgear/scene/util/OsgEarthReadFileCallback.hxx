//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2012  Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthReadFileCallback_h__
#define __OsgEarthReadFileCallback_h__


#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osgEarth/MapNode>
#include <osgEarth/IOTypes>


/**
    * Callback that allows the developer to re-route URI read calls. 
    *
    * If the corresponding callback method returns NOT_IMPLEMENTED, URI will
    * fall back on its default mechanism.
    */
class OsgEarthReadFileCallback : public osgEarth::URIReadCallback
{
public:

    /** 
        * Tells the URI class which data types (if any) from this callback should be subjected
        * to osgEarth's caching mechanism. By default, the answer is "none" - URI
        * will not attempt to read or write from its cache when using this callback.
        */
    virtual unsigned cachingSupport() const { return osgEarth::URIReadCallback::CACHE_NODES; }

public:

    /** Override the readNode() implementation */
    virtual osgEarth::ReadResult readNode(const std::string& uri, const osgDB::Options* options);

public:

    OsgEarthReadFileCallback();

    /** dtor */
    virtual ~OsgEarthReadFileCallback();

protected:
    
    bool FindCachedFile(std::string& filename, std::string& cacheFilename);

    osgEarth::ReadResult OptimizeAndSaveModel(osg::Node* node, const std::string& cacheFilename);
};

#endif
