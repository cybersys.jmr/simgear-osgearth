//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifndef __OsgEarthHeightCache_h__
#define __OsgEarthHeightCache_h__

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include <osg/Referenced>
#include <osg/ref_ptr>
#include <osg/observer_ptr>
#include <osg/CoordinateSystemNode>
#include <osgEarth/MapNode>
#include <osgEarth/Terrain>
#include <osgEarthUtil/GammaColorFilter>
#include <osgEarthUtil/BrightnessContrastColorFilter>

#include <simgear/threads/SGThread.hxx>
#include <simgear/math/SGMath.hxx>

using boost::multi_index::indexed_by;
using boost::multi_index::hashed_unique;
using boost::multi_index::const_mem_fun;
using boost::multi_index::ordered_non_unique;

namespace simgear {

class OsgEarthHeightCell : public osg::Referenced
{
public:
    // constructor
    OsgEarthHeightCell(const std::string name, 
                       const double time, 
                       const double height, 
                       const osg::Vec3d normal)
        :   osg::Referenced(),
            m_Name(name),
            m_Time(time),
            m_HeightAboveMsl(height),
            m_Normal(normal)
    {}

protected:
    // destructor
    ~OsgEarthHeightCell();

public:

    std::string GetName() const { return m_Name; }
    
    double GetTime() const { return m_Time; }

    void GetHeightAndNormal(const double time, double& height, osg::Vec3d& normal)
    {
        // update access time 
        m_Time = time;
        height = m_HeightAboveMsl;
        normal = m_Normal;
    }

protected:

    // cell name
    std::string m_Name;

    // last time accessed (or initially set)
    double m_Time;

    // height above MSL in meters
    double m_HeightAboveMsl;

    osg::Vec3d m_Normal;
};


class OsgEarthHeightCache : public osg::Referenced
{
public:

    // container for a sparse matrix of geographic cells containing height samples.
    // For best performance, a multi_index container is used capable of accessing
    // a cell by name, via associative map, and by time ordered list in order 
    // to remove expired entries.
    typedef boost::multi_index_container<
        osg::ref_ptr<OsgEarthHeightCell>,
        indexed_by<
            // sorted by time
            ordered_non_unique<const_mem_fun<OsgEarthHeightCell, double, &OsgEarthHeightCell::GetTime> >,
            // hash on name
            hashed_unique<const_mem_fun<OsgEarthHeightCell, std::string, &OsgEarthHeightCell::GetName> >
        >
    > OsgEarthHeightCacheContainerType;

    // time sorted list access type
    typedef OsgEarthHeightCacheContainerType::nth_index<0>::type TimeSortedType;

    // associative map access type
    typedef OsgEarthHeightCacheContainerType::nth_index<1>::type MapType;
    

protected:
    // constructor
    OsgEarthHeightCache(const double cellResolutionRad = 0.00001,
                        const unsigned int maxNumEntries = 50)
        :   osg::Referenced(),
            m_SceneGraph(NULL),
            m_Lock(),
            m_CellResolutionRad(cellResolutionRad),
            m_MaxSize(maxNumEntries),
            m_CacheContainer(),
            m_EarthMapNode(NULL),
            m_EarthCoordinateSystemNode(NULL),
            m_Terrain(NULL),
            m_Down(-osg::Z_AXIS),
            m_IsEarthReady(false),
            m_GammaFilterList(),
            m_Gamma(1.0f),
            m_BrightnessContrastFilterList(),
            m_Brightness(1.0f),
            m_Contrast(1.0f),
            m_IsWithinTolerance(false),
            m_IsHeightFieldDirtyFlag(false),
            m_OsgEarthCachePath(""),
            m_HeightFieldPath(""),
            m_IsModelClampDirty(false),
            m_StartModelClampTime(0)
    {}

protected:
    // destructor
    ~OsgEarthHeightCache() 
    {
        m_CacheContainer.clear();
    }

public:

    // singleton access
    static OsgEarthHeightCache* Instance();

    // assign scene graph
    void SetSceneGraph(osg::Node *graph);

    // clear entire height cache
    void ClearCache(); 

    // return scene graph
    osg::Node* GetSceneGraph() { return m_SceneGraph.get(); }

    // return true if earth is ready
    bool IsEarthReady() { return m_IsEarthReady; }

    // set ready status
    void SetIsEarthReady(const bool isReady) { m_IsEarthReady= isReady; }

    // store osg earth file cache path
    void SetOsgEarthCachePath(const char* path) { m_OsgEarthCachePath = path; }

    // return osg earth file cache path
    const char* GetOsgEarthCachePath() { return m_OsgEarthCachePath.c_str(); }

    // set height field path
    void SetHeightFieldPath(const char* path) { m_HeightFieldPath = path; m_IsHeightFieldDirtyFlag = true;}

    // return height field path
    const char* GetHeightFieldPath() { return m_HeightFieldPath.c_str(); }

    // set height field dirty flag
    void SetIsHeightFieldDirtyFlag() { m_IsHeightFieldDirtyFlag = true; }

    // return true if height field dirty flag is set
    bool IsHeightFieldDirtyFlag() { return m_IsHeightFieldDirtyFlag; }

    // add height fields
    void AddHeightFields();

    // ensure earth terrain is initialized
    bool InitEarth();

    // return true if intersection testing is within tolerance
    bool IsWithinTolerance() { return m_IsWithinTolerance; }

    // reset state to effectively ensure that polygon intersection tests match height samples
    void ClearIntersectionToleranceFlag() { m_IsWithinTolerance = false; }

    // retrieve Height above MSL
    bool GetHeightAboveMsl(const double time,
        const SGGeod& geod,
        double& height,
        osg::Vec3d& normal);

    // sample height from elevation sources
    bool SampleHeight(const SGGeod& geod, double& heightAboveMsl, osg::Vec3d& up);

    // fire intersection into earth scene
    bool FireIntersection(
        const osg::Vec3d startCart,
        const osg::Vec3d endCart,
        osg::Vec3d& hitCart,
        double& hamsl,
        osg::Vec3d& worldNormal);

    // find height above terrain using intersection test and / or sample height from sources
    bool FindTerrainHeightInScene(const SGGeod& geod, double& height, osg::Vec3d& normal);

    // set model (KML) clamping state dirty
    void SetIsModelClampingDirty() { m_IsModelClampDirty = true; m_StartModelClampTime = osg::Timer::instance()->tick(); }

    // clear model (KML) clamping state
    void ClearIsModelClampingDirty() { m_IsModelClampDirty = false; }

    // return true if model clamping state is dirty
    bool IsModelClampingDirty() { return m_IsModelClampDirty; }

    // return model clamping start time
    osg::Timer_t GetModelClampingStartTime() { return m_StartModelClampTime; }

    // set true to use coarse resolution sampling
    void SetIsUseCoarseResolution(const bool isCoarse) { m_IsUseCoarseResolution = isCoarse; }

    // return true if using coarse resolution sampling
    bool GetIsUseCoarseResolution() { return m_IsUseCoarseResolution; }

    // set new cell resolution size in radians
    void SetCellResolutionRad(const double cellResolutionRad);

    // return cell resolution size in radians
    double GetCellResolutionRad() { return m_CellResolutionRad; }

    // set max size of cache
    void SetMaxNumEntries(const unsigned int maxSize) { m_MaxSize = maxSize; }

    // return max size of cache
    unsigned int GetMaxNumEntries() { return m_MaxSize; }

    // return current active size of cache
    unsigned int GetActiveSize() { return m_CacheContainer.size(); }

    // compute and return Up vector for a geocentric Cartesian position
    osg::Vec3d GetUpVector(const osg::Vec3d cartPos);

    // compute and return the local coordinate frame for a geocentric Cartesian position
    osg::Matrixd GetFrame(const osg::Vec3d cartPos);

    // return down vector
    osg::Vec3d& GetDown() { return m_Down; }
    
    // set down vector
    void SetDown(const osg::Vec3d down) { m_Down = down; }

    // return Map Node
    osgEarth::MapNode* GetMapNode() { return m_EarthMapNode.get(); }

    // return Coordinate System Node
    osg::CoordinateSystemNode* GetCoordinateSystemNode() { return m_EarthCoordinateSystemNode.get(); }

    // set gamma for all layers
    void SetGamma(const float gamma);

    // return gamma
    float GetGamma() { return m_Gamma; }

    void SetBrightness(const float brightness);
    
    void SetContrast(const float contrast);

    float GetBrightness() { return m_Brightness; }
    
    float GetContrast() { return m_Contrast; }

protected:

    // encode name used for storage / retrieval from cache container
    std::string NameFromGeod(const SGGeod& geod);

    // add height field layer, if layer does not exist
    void AddHeightFieldLayer(const std::string& fullPath, const std::string& layerName);

    // entry to height cache
    void AddCacheEntry(const std::string& accessName, const double time, const double& height, const osg::Vec3d& normal);

    // add gamma image filter to each layeR
    void AddGammaFilter();
 
    // add brightness/contrast image filter to each layer
    void AddBrightnessContrastFilter();
 
    // find coordinate system node, return true if successful
    bool FindCoordinateSystemNode();

protected:

    // input scene graph
    osg::observer_ptr<osg::Node> m_SceneGraph;

    // lock for osgEarth initialization called from multiple threads
    SGMutex m_Lock;

    // maximum size of cache
    unsigned int m_MaxSize;

    // OsgEarth's file cache path
    std::string m_OsgEarthCachePath;

    // height field path
    std::string m_HeightFieldPath;

    // true if height field path has been updated
    bool m_IsHeightFieldDirtyFlag;

    // true if using coarse resolution sampling
    bool m_IsUseCoarseResolution;

    // cell resolution in radians
    double m_CellResolutionRad;

    // contains the cache
    OsgEarthHeightCacheContainerType m_CacheContainer;

    // earth coordinate system node useful for determining
    // local coordinate frame for a give point in space
    osg::observer_ptr<osg::CoordinateSystemNode> m_EarthCoordinateSystemNode;

    // earth root map node subgraph
    osg::observer_ptr<osgEarth::MapNode> m_EarthMapNode;

    // earth terrain scene subgraph
    osg::observer_ptr<osgEarth::Terrain> m_Terrain;

    // local coordinate frame's current down direction
    osg::Vec3d m_Down;

    // true if intersection tests match height samples
    bool m_IsWithinTolerance;

    // true if earth is ready for queries
    bool m_IsEarthReady;

    // list of gamma filters for each layer
    std::vector<osg::ref_ptr<osgEarth::Util::GammaColorFilter> > m_GammaFilterList;

    // current layer gamma
    float m_Gamma;

    // list of brightness/contrast filters for each layer
    std::vector<osg::ref_ptr<osgEarth::Util::BrightnessContrastColorFilter> > m_BrightnessContrastFilterList;

    // current layer brightness
    float m_Brightness;

    // current layer contrast
    float m_Contrast;

    // true if model clamping state is dirty
    bool m_IsModelClampDirty;

    // time since model clamping went dirty
    osg::Timer_t m_StartModelClampTime;




private:
    // singleton instance
    static osg::ref_ptr<OsgEarthHeightCache> s_Instance;

};

}

#endif
