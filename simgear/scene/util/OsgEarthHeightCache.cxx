//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#include <boost/format.hpp>
#include <osg/Math>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <osgEarth/Registry>
#include <osgEarth/ElevationQuery>
#include <osgEarth/NodeUtils>
#include <osgEarthDrivers/gdal/GDALOptions>
#include <osgUtil/LineSegmentIntersector>

#include <simgear/scene/util/OsgMath.hxx>
#include <simgear/debug/logstream.hxx>
#include <simgear/scene/util/OsgEarthReadFileCallback.hxx>
#include <simgear/scene/util/OsgEarthModelLoad.hxx>
#include <simgear/threads/SGGuard.hxx>

#include <simgear/scene/util/OsgEarthHeightCache.hxx>


using namespace simgear;

// static singleton instance
osg::ref_ptr<OsgEarthHeightCache> OsgEarthHeightCache::s_Instance = NULL;

// return the single instance of this class
OsgEarthHeightCache* OsgEarthHeightCache::Instance() {
    if (!s_Instance.valid()) {
        s_Instance = new OsgEarthHeightCache();
    }
    return s_Instance.get();
}

// encode name used for storage / retrieval from cache container
std::string OsgEarthHeightCache::NameFromGeod(const SGGeod& geod)
{
    std::string ret = "";
    
    // convert to degrees for readability
    double latDeg = geod.getLatitudeDeg();
    double longDeg = geod.getLongitudeDeg();

    bool isWest = longDeg < 0.0;
    unsigned int longIndex = 
        (unsigned int)(fabs(longDeg) / m_CellResolutionRad) + (isWest ? 1 : 0);

    bool isSouth = latDeg < 0.0;
    unsigned int latIndex =
        (unsigned int)(fabs(latDeg) / m_CellResolutionRad) + (isSouth ? 1 : 0);

    ret = (boost::format("%d%c_%d%c")
        % latIndex
        % (isWest ? "W" : "E")
        % longIndex
        % (isSouth ? "S" : "N")).str();

    return ret;
}


void OsgEarthHeightCache::SetSceneGraph(osg::Node *graph)
{
    if (m_SceneGraph != graph) {
        
        m_CacheContainer.clear();
        m_EarthMapNode = NULL;
        m_EarthCoordinateSystemNode = NULL;
        m_Terrain = NULL;
        m_GammaFilterList.clear();
        m_BrightnessContrastFilterList.clear();
        m_IsHeightFieldDirtyFlag = true;

        // update scene graph
        m_SceneGraph = graph;

        // attempt to find earth
        InitEarth();
    }
}

void OsgEarthHeightCache::AddCacheEntry(const std::string& accessName, 
                                        const double time, 
                                        const double& height,
                                        const osg::Vec3d& normal)
{
    // add entry to cache container
    m_CacheContainer.insert(
        new OsgEarthHeightCell(accessName, time, height, normal));

    // ensure max size is reasonable
    m_MaxSize = osg::maximum((unsigned int)(2), m_MaxSize);

    // --- If container size too large, remove oldest ---

    if (m_CacheContainer.size() > m_MaxSize) {

        TimeSortedType& list = m_CacheContainer.get<0>();

        // oldest entries are on the front of the list
        while (m_CacheContainer.size() > m_MaxSize) {
            TimeSortedType::iterator it = list.begin(); 
            m_CacheContainer.erase(it);
        }
    }
}

bool OsgEarthHeightCache::FindTerrainHeightInScene(const SGGeod& geod,
                                                   double& height,
                                                   osg::Vec3d& normal)
{
    bool isHit = false;

    const double sampleMagnitude = 20000.0;

    // sample vector in geodetic frame
    SGGeod startGeod = geod.fromGeodM(geod, sampleMagnitude);
    SGGeod endGeod = geod.fromGeodM(geod, -sampleMagnitude);

    // sample vector in geocentric frame
    osg::Vec3d startCart = toOsg(SGVec3d::fromGeod(startGeod));
    osg::Vec3d endCart = toOsg(SGVec3d::fromGeod(endGeod));

    osg::Vec3d hitCart;

    // perform polygonal intersection test
    isHit = FireIntersection(startCart, endCart, hitCart, height, normal);

    if ((m_IsWithinTolerance == false) || (isHit == false)) {

        // --- no intersection or quality is questionable ---

        // save intersection test results to compare quality
        double isectHeight = height;

        // sample height from available elevation sources 
        // (slower than polygon intersection test)
        isHit = SampleHeight(geod, height, normal);

        if (m_IsWithinTolerance == false) {
            // eplison comparison
            double hdiff = height - isectHeight;

            const float nearThresholdEplison = 0.5f;
            if (fabs(hdiff) < nearThresholdEplison) {
                // quality is now acceptable
                m_IsWithinTolerance = true;
                SG_LOG(SG_IO, SG_ALERT, 
                    "[OsgEarthHeightCache] Height queries ARE now within tolerance");
            }
        }
    }

    return isHit;
}

// retrieve Height above MSL and normal vector
bool OsgEarthHeightCache::GetHeightAboveMsl(const double time,
                                            const SGGeod& geod,
                                            double& height,
                                            osg::Vec3d& normal)
{
    bool ret = false;

    if (m_EarthMapNode != NULL) {

        std::string accessName = NameFromGeod(geod);

        MapType& nameIndex = m_CacheContainer.get<1>();
        MapType::iterator i = nameIndex.find(accessName);

        if (i != nameIndex.end()) {

            // reference the cell as entry will be temporarily erased from container
            osg::ref_ptr<OsgEarthHeightCell> cell = i->get();
            if (cell != NULL) {
            
                // retrieve height above MSL and normal vector, update access time
                cell->GetHeightAndNormal(time, height, normal);

                ret = true;
            }

        } else {

            // --- position is not in height cache, so sample height ---

            if ((m_Terrain != NULL) && 
                m_Terrain->getProfile()->getExtent().contains(
                    geod.getLongitudeRad(), 
                    geod.getLatitudeRad())) 
            {
                // terrain contains position, retrieve height from scene 
                bool isFound = FindTerrainHeightInScene(geod, height, normal);

                if (isFound) {

                    // add entry to cache
                    AddCacheEntry(accessName, time, height, normal);

                    ret = true;
                }
            }
        }
    }

    return ret;
}

// clear entire height cache
void OsgEarthHeightCache::ClearCache()
{
    m_CacheContainer.clear();
}


// set new cell resolution size in radians
void OsgEarthHeightCache::SetCellResolutionRad(const double cellResolutionRad)
{
    if (cellResolutionRad != m_CellResolutionRad) {
        m_CacheContainer.clear();
        m_CellResolutionRad = cellResolutionRad;
    }
}

// fire intersection test into polygonal scene
bool OsgEarthHeightCache::FireIntersection(const osg::Vec3d startCart,
                                           const osg::Vec3d endCart,
                                           osg::Vec3d& hitCart,
                                           double& hamsl,
                                           osg::Vec3d& worldNormal)
{
    bool ret = false;

    if ((m_Terrain != NULL) && (m_Terrain->getGraph() != NULL)) {

        osgUtil::LineSegmentIntersector* lsi = 
            new osgUtil::LineSegmentIntersector(startCart, endCart);
        osgUtil::IntersectionVisitor iv(lsi);
        iv.setLODSelectionMode(osgUtil::IntersectionVisitor::USE_HIGHEST_LEVEL_OF_DETAIL);

        m_Terrain->getGraph()->accept(iv);

        if (!lsi->getIntersections().empty()) {

            // get first intersection
            osgUtil::LineSegmentIntersector::Intersection hitIsect = 
                lsi->getFirstIntersection();
            
            // hit point (geocentric)
            hitCart = hitIsect.getWorldIntersectPoint();
    
            double hae;
            osg::Vec3d lla;
            // transform from world (geocentric) to height above terrain
            m_Terrain->getSRS()->transformFromWorld(hitCart, lla, &hae);
            hamsl = lla.z();

            // geocentric normal
            worldNormal = hitIsect.getWorldIntersectNormal();

            ret = true;
        }
    }

    return ret;
}

// sample height above MSL from available elevation sources
bool OsgEarthHeightCache::SampleHeight(const SGGeod& geod,
                                       double& heightAboveMsl,
                                       osg::Vec3d& normal)
{
    bool ret = false;

    // continually ensure earth terrain is initialized
    InitEarth();

    if (m_EarthCoordinateSystemNode != NULL) {

        double sampledHamsl = 0.0;

        // convert from geodetic back to geocentric Cartesian 
        osg::Vec3d cart = toOsg(SGVec3d::fromGeod(geod));

        // --- sample height from available elevation sources ---
        double sampleResolution = 0.0001;
        double actualResolution = 0.0;
        osgEarth::ElevationQuery elevQuery(m_EarthMapNode->getMap());
        elevQuery.setMaxTilesToCache(10);

        osgEarth::GeoPoint point(
            m_EarthMapNode->getMapSRS(), 
            osg::Vec3d(geod.getLongitudeDeg(), 
                       geod.getLatitudeDeg(),
                       0.0),
            osgEarth::ALTMODE_ABSOLUTE);

        bool foundHaE = elevQuery.getElevation(
            point,
            sampledHamsl,
            sampleResolution, 
            &actualResolution);
        
        if (foundHaE) {
            ret = true;

            heightAboveMsl = sampledHamsl;

            osg::Vec3d up = m_EarthCoordinateSystemNode->computeLocalUpVector(cart);

            // update local down vector
            m_Down = -up;
            
            // let normal vector be the local up vector
            normal = up;
        }
    } 

    return ret;
}

// set gamma for all layers
void OsgEarthHeightCache::SetGamma(const float gamma)
{
    m_Gamma = gamma;
    for (unsigned int i = 0; i < m_GammaFilterList.size(); ++i) {
        m_GammaFilterList[i]->setGamma(m_Gamma);
    }
}

// set brightness for all layers
void OsgEarthHeightCache::SetBrightness(const float brightness)
{
    m_Brightness = brightness;
    for (unsigned int i = 0; i < m_BrightnessContrastFilterList.size(); ++i) {
        m_BrightnessContrastFilterList[i]->setBrightnessContrast(osg::Vec2(m_Brightness, m_Contrast));
    }
}

// set contrast for all layers
void OsgEarthHeightCache::SetContrast(const float contrast)
{
    m_Contrast = contrast;
    for (unsigned int i = 0; i < m_BrightnessContrastFilterList.size(); ++i) {
        m_BrightnessContrastFilterList[i]->setBrightnessContrast(osg::Vec2(m_Brightness, m_Contrast));
    }
}

void OsgEarthHeightCache::AddHeightFieldLayer(const std::string& fullPath, 
                                              const std::string& layerName)
{
    if (m_EarthMapNode->getMap()->getElevationLayerByName(layerName) == NULL) {

        // -- height field is not a layer yet, create it ---

        osgEarth::Drivers::GDALOptions gdal;
        gdal.tileSize() = 30;
        gdal.url() = fullPath;
        osgEarth::ElevationLayerOptions layeropt(layerName, gdal); 
        layeropt.cacheId() = "heightFields";
        
        // create layer
        osgEarth::ElevationLayer* layer = 
            new osgEarth::ElevationLayer(layeropt);

        
        m_EarthMapNode->getMap()->addElevationLayer(layer);
        SG_LOG(SG_TERRAIN, SG_INFO, 
            "[OsgEarthHeightCache] Added HeightField layer to OsgEarth: " 
            << layerName << std::endl);
    }
}

void OsgEarthHeightCache::AddHeightFields()
{
    if (m_IsEarthReady && m_IsHeightFieldDirtyFlag) {

        // clear flag
        m_IsHeightFieldDirtyFlag = false;

        // --- add height field layers ---

        // get contents of directory
        osgDB::DirectoryContents contents = osgDB::getDirectoryContents(m_HeightFieldPath);

        for (unsigned int i = 0; i < contents.size(); ++i) {

            std::string nameWithPath = m_HeightFieldPath + "/";
            nameWithPath += contents[i];

            std::string layerName = osgDB::getNameLessExtension(contents[i]);

            if (osgDB::fileType(nameWithPath) == osgDB::REGULAR_FILE) {
                // add new layer (if not already added)
                AddHeightFieldLayer(nameWithPath, layerName);
            }
        }

        // --- force KML entity reclamp ---

        simgear::OsgEarthHeightCache::Instance()->SetIsModelClampingDirty();
        SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthHeightCache] marking model clamping as dirty" << std::endl);
    }
}

void OsgEarthHeightCache::AddGammaFilter()
{
    // --- add gamma image filter to each layer ---

    if (m_GammaFilterList.size() == 0) {
        // attach color filter to each layer.
        unsigned numLayers = m_EarthMapNode->getMap()->getNumImageLayers();
        for(unsigned i = 0; i < numLayers; ++i) {
            osgEarth::ImageLayer* layer = m_EarthMapNode->getMap()->getImageLayerAt(i);
            osgEarth::Util::GammaColorFilter* filter = new osgEarth::Util::GammaColorFilter();
            m_GammaFilterList.push_back(filter);
            layer->addColorFilter(filter);
            filter->setGamma(m_Gamma);
        }
    }
}

void OsgEarthHeightCache::AddBrightnessContrastFilter()
{
    // --- add brightness/contrast image filter to each layer ---

    if (m_BrightnessContrastFilterList.size() == 0) {

        // attach color filter to each layer.
        unsigned numLayers = m_EarthMapNode->getMap()->getNumImageLayers();
        for(unsigned i = 0; i < numLayers; ++i) {
            osgEarth::ImageLayer* layer = m_EarthMapNode->getMap()->getImageLayerAt(i);
            osgEarth::Util::BrightnessContrastColorFilter* filter =
                new osgEarth::Util::BrightnessContrastColorFilter();
            m_BrightnessContrastFilterList.push_back(filter);
            layer->addColorFilter(filter);
            filter->setBrightnessContrast(osg::Vec2(m_Brightness, m_Contrast));
        }
    }
}

bool OsgEarthHeightCache::FindCoordinateSystemNode()
{
    bool ret = false;

    // --- find coordinate system node, return true if found ---

    if ((m_EarthMapNode->getTerrainEngine() != NULL) && 
        (m_EarthMapNode->getTerrain() != NULL))
    {
        if (m_Terrain == NULL) {
            m_Terrain = m_EarthMapNode->getTerrain();
        }
        if ((m_Terrain != NULL) && 
            (m_Terrain.valid()) && 
            (m_EarthCoordinateSystemNode == NULL)) 
        {
            m_EarthCoordinateSystemNode = 
                osgEarth::findTopMostNodeOfType<osg::CoordinateSystemNode>(
                m_Terrain->getGraph());
        }

        if (m_EarthCoordinateSystemNode != NULL) {
            ret = true;
        }
    }

    return ret;
}

bool OsgEarthHeightCache::InitEarth()
{
    // --- ensure earth scene graph has been initialized ---

    // protect state from multiple threads 
    SGGuard<SGMutex> g(m_Lock);

    bool ret = false;

    if (m_SceneGraph != NULL) {
        if ((m_EarthMapNode != NULL) && (m_EarthMapNode.valid() == false)) {
            m_EarthMapNode = NULL;
            m_EarthCoordinateSystemNode = NULL;
        }

        if (m_EarthMapNode == NULL) {
            m_EarthMapNode = osgEarth::MapNode::findMapNode(m_SceneGraph.get());
        }

        if (m_EarthMapNode != NULL) {

            // find coordinate system node
            ret = FindCoordinateSystemNode();

            // add gamma image filter to each layer
            AddGammaFilter();

            // add brightness/contrast image filter to each layer
            AddBrightnessContrastFilter();

            // ensure height fields are re-added
            AddHeightFields();
        }
    }

    return ret;
}

// destructor
OsgEarthHeightCell::~OsgEarthHeightCell()
{
    // exists for debugging object destruction
}

// compute and return Up vector for a geocentric Cartesian position
osg::Vec3d OsgEarthHeightCache::GetUpVector(const osg::Vec3d cartPos)
{
    osg::Vec3d ret(0.0, 0.0, 0.0);

    if (m_EarthCoordinateSystemNode != NULL) {
        ret = m_EarthCoordinateSystemNode->computeLocalUpVector(cartPos);
    }

    return ret;
}

// compute and return the local coordinate frame for a geocentric Cartesian position
osg::Matrixd OsgEarthHeightCache::GetFrame(const osg::Vec3d cartPos)
{
    osg::Matrixd ret = osg::Matrixd::identity();

    if (m_EarthCoordinateSystemNode != NULL) {
        ret = m_EarthCoordinateSystemNode->computeLocalCoordinateFrame(cartPos);
    }

    return ret;
}
