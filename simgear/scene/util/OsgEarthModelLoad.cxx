//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#include <simgear/scene/util/OsgEarthModelLoad.hxx>

#include <osgEarthDrivers/kml/KML>
#include <osgEarthDrivers/kml/KMLOptions>
#include <osgDB/FileNameUtils>
#include <osgDB/FileUtils>
#include <simgear/scene/util/OsgEarthHeightCache.hxx>
#include <simgear/scene/util/OsgEarthReadFileCallback.hxx>

osgDB::DirectoryContents GetSubDirList(const std::string& inDirName)
{
    osgDB::DirectoryContents subdirList;

    // get contents of directory (filenames and/or sub directories)
    osgDB::DirectoryContents contents = osgDB::getDirectoryContents(inDirName);

    for (unsigned int i = 0; i < contents.size(); ++i) {
        std::string nameWithPath = inDirName + "/";
        nameWithPath += contents[i];
        if (osgDB::fileType(nameWithPath) == osgDB::DIRECTORY) {
            if ((contents[i] != ".") && (contents[i] != "..")) {
                std::string subdirName = inDirName + "/";
                subdirName += contents[i];
                subdirList.push_back(subdirName);
            }
        }
    }

    return subdirList;
}

void GetFileList(const std::string& inDirName, 
    osgDB::DirectoryContents& fileList,
    const bool recurse)
{
    if (recurse) {
        // get list of sub-directories
        osgDB::DirectoryContents subdirList = GetSubDirList(inDirName);

        for (unsigned int i = 0; i < subdirList.size(); ++i) {
            // recurse
            GetFileList(subdirList[i], fileList, recurse);
        }
    }

    // get contents of directory (filenames and/or sub directories)
    osgDB::DirectoryContents contents = osgDB::getDirectoryContents(inDirName);

    for (unsigned int i = 0; i < contents.size(); ++i) {
        std::string nameWithPath = inDirName + "/";
        nameWithPath += contents[i];

        if (osgDB::fileType(nameWithPath) == osgDB::REGULAR_FILE) {
            // add filename (with complete path) to list
            fileList.push_back(nameWithPath);
        }
    }
}


namespace simgear {

// static singleton instance
osg::ref_ptr<OsgEarthModelLoad> OsgEarthModelLoad::s_Instance = NULL;

// return the single instance of this class
OsgEarthModelLoad* OsgEarthModelLoad::Instance() {
    if (!s_Instance.valid()) {
        s_Instance = new OsgEarthModelLoad();
    }
    return s_Instance.get();
}

osg::Group* OsgEarthModelLoad::LoadKMLmodels(const std::string& path)
{    
    osg::Group* root = NULL;

    m_InputPath = path; 

    // --- load 3D models (KML) into the scene ---

    if (osgEarth::Registry::instance()->getURIReadCallback() == NULL) {

        // install a custom read file callback for KML file loading
        osgEarth::Registry::instance()->setURIReadCallback(new OsgEarthReadFileCallback());

        osgDB::DirectoryContents fileList;
        const bool doRecurse = true;

        if (path.length() > 0) {
            GetFileList(path, fileList, doRecurse);

            osgEarth::MapNode* mapNode = dynamic_cast<osgEarth::MapNode*>(
                simgear::OsgEarthHeightCache::Instance()->GetMapNode());

            if (mapNode != NULL) {

                root = new osg::Group();
                root->setName("KML root");
                osg::ref_ptr<osg::Group> kmlRoot = new osg::Group;
                root->addChild(kmlRoot);

                for (unsigned int i = 0; i < fileList.size(); ++i) {

                    std::string ext = osgDB::getLowerCaseFileExtension(fileList[i].c_str());
                    if (ext == "kml") {

                        osgEarth::Drivers::KMLOptions kml_options;

                        osg::ref_ptr<osg::Node> kml = osgEarth::Drivers::KML::load(
                            osgEarth::URI(fileList[i].c_str()), mapNode, kml_options);
                        if (kml != NULL) {
                            if (kmlRoot != NULL) {
                                kmlRoot->addChild(kml);
                            }
                        }
                    }
                }
            }
        }

        // clear callback
        osgEarth::Registry::instance()->setURIReadCallback(NULL);
    }

    return root;
}

}
