//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#include <simgear/scene/util/OsgEarthUtil.hxx>

#ifdef ENABLE_OSGEARTH
#   include <simgear/scene/util/SGNodeMasks.hxx>
#   include <simgear/scene/util/OsgEarthModelLoad.hxx>
#   include <simgear/scene/util/OsgEarthHeightCache.hxx>

    using namespace simgear;
#endif


void monitorIsEarthReady()
{
#   ifdef ENABLE_OSGEARTH
        // --- ensure that OsgEarth is marked as ready once all input processing is complete ---
        if ((OsgEarthHeightCache::Instance()->GetCoordinateSystemNode() != NULL) && 
            (OsgEarthHeightCache::Instance()->GetMapNode() != NULL))
        {
            OsgEarthHeightCache::Instance()->SetIsEarthReady(true);
        } else {
            OsgEarthHeightCache::Instance()->SetIsEarthReady(false);
        }
#   endif
}

bool isEarthReady()
{
    bool ret = false;

#   ifdef ENABLE_OSGEARTH
        
        ret = OsgEarthHeightCache::Instance()->IsEarthReady();

#   endif

    return ret;
}

void loadKmlModels(const char* path)
{
#   ifdef ENABLE_OSGEARTH
        // load KML models
        osg::Group* kmlModels = OsgEarthModelLoad::Instance()->LoadKMLmodels(path);
        if (OsgEarthHeightCache::Instance()->GetMapNode() != NULL) {
            OsgEarthHeightCache::Instance()->GetMapNode()->addChild(kmlModels);
        }
#   endif
}

osg::Node* GetEarthScene() 
{
    osg::Node* ret = NULL;

#   ifdef ENABLE_OSGEARTH
        ret = OsgEarthHeightCache::Instance()->GetSceneGraph();
#   endif

    return ret;
}

void setEarthScene(osg::Node* node)
{
#   ifdef ENABLE_OSGEARTH

        node->setName("OsgEarthRoot");
        node->setNodeMask(SG_NODEMASK_TERRAIN_BIT | node->getNodeMask());
        OsgEarthHeightCache::Instance()->SetSceneGraph(node);

#   endif
}
