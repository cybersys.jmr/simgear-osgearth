//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$


#include <osg/Texture3D>
#include <osg/Material>
#include <osg/GLObjects>
#include <osg/ComputeBoundsVisitor>
#include <osg/CameraView>
#include <simgear/scene/util/OsgEarthReadFileCallback.hxx>
#include <simgear/scene/util/OsgEarthHeightCache.hxx>
#include <simgear/scene/util/OsgEarthModelLoad.hxx>
#include <simgear/misc/sg_path.hxx>
#include <simgear/debug/logstream.hxx>
#include <osgDB/FileUtils>
#include <osgDB/FileNameUtils>
#include <osgDB/WriteFile>
#include <osgViewer/Viewer>
#include <osg/Texture2D>
#include <osg/MatrixTransform>
#include <osgUtil/Simplifier>
#include <osgUtil/Optimizer>


///////////////////////////////////////////////////////////////////////////////
// local class definition: LocalGraphicsContext
// Description: used during model scene graph optimization to temporarily 
//              create an OpenGL context necessary to compress textures after 
//              they have already been loaded from media.
//
class LocalGraphicsContext : public osg::Referenced {
public:
    LocalGraphicsContext()
        : osg::Referenced()
    {

        osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits;
        traits->x = 0;
        traits->y = 0;
        traits->width = 1;
        traits->height = 1;
        traits->windowDecoration = false;
        traits->doubleBuffer = false;
        traits->sharedContext = 0;
        traits->pbuffer = true;

        if (osg::GraphicsContext::getMaxContextID() > 0) {
            osg::GraphicsContext::GraphicsContexts contexts = osg::GraphicsContext::getAllRegisteredGraphicsContexts();
            // share an active context
            traits->sharedContext = contexts[0];
        }

        m_Context = osg::GraphicsContext::createGraphicsContext(traits.get());

        if (m_Context.valid()) {
            m_Context->realize();
            m_Context->makeCurrent();
            SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthReadFileCallback] Realized temporary local OpenGL context" << std::endl);
        }
    }

    ~LocalGraphicsContext()
    {
        // --- cleanup ---

        unsigned int id = m_Context->getState()->getContextID();
        osg::deleteAllGLObjects(id);

        osg::flushAllDeletedGLObjects(id);

        osg::discardAllGLObjects(id);

        osgDB::Registry::instance()->clearObjectCache();

        osgDB::Registry::instance()->clearArchiveCache();

        if (m_Context != NULL) {
            m_Context->close();
        }

    }

    bool valid() const { return m_Context.valid() && m_Context->isRealized(); }

private:
    osg::ref_ptr<osg::GraphicsContext> m_Context;
};



///////////////////////////////////////////////////////////////////////////////
// local class definition: OptimizeModelVisitor
// Description: class dedicated to optimizing a model's scene graph including:
//              texture compression and texture size limiting.
//
class OptimizeModelVisitor : public osg::NodeVisitor
{
public:

    OptimizeModelVisitor():
      osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN) {}

      void AdjustMaterial(osg::StateSet* ss)   
      {
          // --- brighten material a bit ---
          osg::Material *material = dynamic_cast<osg::Material*>
              (ss->getAttribute(osg::StateAttribute::MATERIAL));
          if (material != NULL) {
              material->setAmbient(osg::Material::FRONT, 
                  osg::Vec4(0.3, 0.3, 0.3, 1.0));
              material->setDiffuse(osg::Material::FRONT, 
                  osg::Vec4(0.9, 0.9, 0.9, 1.0));
              material->setSpecular(osg::Material::FRONT,  
                  osg::Vec4(0.7, 0.7, 0.7, 1.0));
              ss->setAttributeAndModes(material); 
          }
      }

      inline virtual void apply(osg::Node& node)
      {
          if (node.getStateSet()) {
              AdjustMaterial(node.getStateSet());
          }

          if (node.getStateSet()) apply(*node.getStateSet());
          traverse(node);
      }

      inline virtual void apply(osg::Geode& node)
      {
          if (node.getStateSet()) {
              AdjustMaterial(node.getStateSet());
          }

          if (node.getStateSet()) apply(*node.getStateSet());

          for(unsigned int i=0;i<node.getNumDrawables();++i)
          {
              osg::Drawable* drawable = node.getDrawable(i);
              if (drawable && drawable->getStateSet()) apply(*drawable->getStateSet());
          }

          traverse(node);
      }

      inline virtual void apply(osg::StateSet& stateset)
      {
          AdjustMaterial(&stateset);

          // search for the existence of any texture object attributes
          for(unsigned int i = 0; i < stateset.getTextureAttributeList().size(); ++i) {
              osg::Texture* texture = dynamic_cast<osg::Texture*>
                  (stateset.getTextureAttribute(i,osg::StateAttribute::TEXTURE));
              if (texture) {
                  _textureSet.insert(texture);
              }
          }
      }

      void EnsureValidSize(const unsigned int maxTexSize) 
      {
          osg::ref_ptr<osg::State> state = new osg::State;

          for(TextureSet::iterator itr=_textureSet.begin();
              itr!=_textureSet.end();
              ++itr)
          {
              osg::Texture* texture = const_cast<osg::Texture*>(itr->get());
              osg::Texture2D* texture2D = dynamic_cast<osg::Texture2D*>(texture);
              osg::Texture3D* texture3D = dynamic_cast<osg::Texture3D*>(texture);
              osg::ref_ptr<osg::Image> image = texture2D ? texture2D->getImage() : 
                  (texture3D ? texture3D->getImage() : 0);
              if (image != NULL) {
                  image->ensureValidSizeForTexturing(maxTexSize);
              }
          }
      }

      // compress textures
      void Compress(osg::Texture::InternalFormatMode formatMode)
      {
          osg::ref_ptr<osg::State> state = new osg::State;

          SG_LOG(SG_TERRAIN, SG_INFO, "Compressing textures..." << std::endl);

          for(TextureSet::iterator itr=_textureSet.begin();
              itr!=_textureSet.end();
              ++itr)
          {
              osg::Texture* texture = const_cast<osg::Texture*>(itr->get());

              osg::Texture2D* texture2D = dynamic_cast<osg::Texture2D*>(texture);
              osg::Texture3D* texture3D = dynamic_cast<osg::Texture3D*>(texture);

              osg::ref_ptr<osg::Image> image = texture2D ? texture2D->getImage() : 
                  (texture3D ? texture3D->getImage() : 0);
              if ((image != NULL) && 
                  (image->getPixelFormat() == GL_RGB || image->getPixelFormat()==GL_RGBA) &&
                  (image->s()>=32 && image->t()>=32))
              {

                  // print texture name
                  SG_LOG(SG_TERRAIN, SG_INFO, "    " 
                      << osgDB::getSimpleFileName(image->getFileName()).c_str() << std::endl);

                  texture->setInternalFormatMode(formatMode);

                  // need to disable the unref after apply, otherwise the image could go out of scope.
                  bool unrefImageDataAfterApply = texture->getUnRefImageDataAfterApply();
                  texture->setUnRefImageDataAfterApply(false);

                  // get OpenGL driver to create texture from image
                  texture->apply(*state);

                  // restore the original setting
                  texture->setUnRefImageDataAfterApply(unrefImageDataAfterApply);

                  image->readImageFromCurrentTexture(0, true);

                  texture->setInternalFormatMode(osg::Texture::USE_IMAGE_DATA_FORMAT);
              }
          }
      }

      typedef std::set< osg::ref_ptr<osg::Texture> > TextureSet;
      TextureSet _textureSet;

};

osg::Node* ScaleGraph(osg::Node *graph, float scale)
{
    osg::Node* root = graph;

    // uniformly scale scene graph

    SG_LOG(SG_TERRAIN, SG_INFO, "applying model scaling..." << std::endl);

    if (scale != 1.0f) {
        osg::MatrixTransform* scaleNode = new osg::MatrixTransform();
        osg::Matrix scaleMat = osg::Matrix::identity();
        scaleMat.makeScale(osg::Vec3(scale, scale, scale));
        scaleNode->setMatrix(scaleMat);
        osg::StateSet* ss = scaleNode->getOrCreateStateSet();
        ss->setMode(GL_RESCALE_NORMAL, osg::StateAttribute::ON|osg::StateAttribute::OVERRIDE);

        osg::Group* rootG = dynamic_cast<osg::Group*> (root);
        scaleNode->addChild(rootG);

        // compute bounds with scale applied
        osg::ComputeBoundsVisitor ev(osg::NodeVisitor::TRAVERSE_ACTIVE_CHILDREN);
        ev.reset();
        scaleNode->accept(ev);

        SG_LOG(SG_TERRAIN, SG_INFO, "Post Scale --- center: " << 
            ev.getBoundingBox().center().x() << " " << 
            ev.getBoundingBox().center().y() << " " << 
            ev.getBoundingBox().center().z() << 
            " minZ: " << ev.getBoundingBox().zMin() <<
            " maxX: " << ev.getBoundingBox().zMax() << std::endl);

        rootG = scaleNode;
        root = rootG;
    }

    return root;
}

OsgEarthReadFileCallback::OsgEarthReadFileCallback()
{
}

OsgEarthReadFileCallback::~OsgEarthReadFileCallback()
{
}


bool OsgEarthReadFileCallback::FindCachedFile(std::string& filename, 
                                              std::string& cacheFilename)
{
    bool isCachedFileExists = false;

    std::string baseName = osgDB::getSimpleFileName(osgDB::getNameLessExtension(filename)) + ".ive";

    std::string kmlPath = simgear::OsgEarthModelLoad::Instance()->GetInputPath();
    std::string diff = osgDB::getPathRelative(
        kmlPath,
        osgDB::getFilePath(filename));

    std::string root = simgear::OsgEarthHeightCache::Instance()->GetOsgEarthCachePath();
    root += "/ModelCache";

    cacheFilename = root + "/" + diff + "/" + baseName;

    isCachedFileExists = osgDB::fileExists(cacheFilename);

    if (isCachedFileExists) {
        // file exists, re-construct name based on cache file
        filename = cacheFilename; 

    } else {
        // files does not exist, so ensure folder exists
        SGPath sgPath = osgDB::getFilePath(cacheFilename);

        if (sgPath.isDir() == false) {
            // create folder
            sgPath.add("/");
            sgPath.create_dir(0755);
        }
    }

    return isCachedFileExists;
}

osgEarth::ReadResult OsgEarthReadFileCallback::OptimizeAndSaveModel(osg::Node* node, 
                                                                    const std::string& cacheFilename)
{
    osgEarth::ReadResult ret = node;

    // create a graphics context for texture scaling and compression 
    osg::ref_ptr<LocalGraphicsContext> gfxContext = new LocalGraphicsContext();
    if (gfxContext != NULL) {

        OptimizeModelVisitor optVisitor;
        node->accept(optVisitor);
        const unsigned int maxTexSize = 512;
        optVisitor.EnsureValidSize(maxTexSize);
        optVisitor.Compress(osg::Texture::USE_ARB_COMPRESSION);

        // simplify model geometry and ensure model has normals (smoothing)
        osgUtil::Simplifier simplifier;
        simplifier.setSampleRatio(0.1);
        simplifier.setSmoothing(true);
        node->accept(simplifier);

        osgUtil::Optimizer optimizer;
        optimizer.optimize(node, osgUtil::Optimizer::ALL_OPTIMIZATIONS);

        // wrap an LOD node around the model
        osg::ref_ptr<osg::LOD> lod = new osg::LOD();
        lod->setRangeMode(osg::LOD::DISTANCE_FROM_EYE_POINT);
        lod->setName("KML lod");
        lod->addChild(node);
        lod->setRange(0, 20.0f, 10000.0f);

        // return model with LOD at root
        ret = lod.get();

        // --- save to cache ---

        SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthReadFileCallback] saving cache: " <<
            cacheFilename << std::endl);

        if (osgDB::writeNodeFile(*lod, cacheFilename) == false) {
            SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthReadFileCallback] cache file save failed" << std::endl);
        }
    }

    return ret;
}

osgEarth::ReadResult OsgEarthReadFileCallback::readNode(const std::string& uri, 
                                                        const osgDB::Options* options)
{
    // override readNode functionality in order to improve load times.
    // captures KML file reads in hopes of optimizing and caching to ive 

    osgEarth::ReadResult ret = osgEarth::ReadResult::RESULT_NOT_FOUND;

    // check for cached ive file
    std::string filename = uri;
    std::string ext = osgDB::getLowerCaseFileExtension(filename);
    std::string cacheFilename = "";
    bool isCachedFileExists = false;

    if (ext != "ive") {
        // locate cached "ive" and update filename if available, return cache filename
        isCachedFileExists = FindCachedFile(filename, cacheFilename);
    }

    if (osgDB::getLowerCaseFileExtension(filename) == "dae") {
        // loading a Collada "dae", enable option flag to convert to meters
        options->setPluginData("DAE-AssetUnitMeter",  new float());
    }

    // read the input file from disk
    osg::Node* node = osgDB::readNodeFile(filename, options);

    if (node != NULL) {

        // successfully read file
        ret = node;

        if ((ext == "dae") && isCachedFileExists) {
            SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthReadFileCallback] loaded cached model file: "  << filename << std::endl);
        } 
        
        if ((ext == "dae") && (isCachedFileExists == false)) {
            
            // --- model is Collada "dae" and cache does not exist yet ---
            SG_LOG(SG_TERRAIN, SG_INFO, "[OsgEarthReadFileCallback] loaded Collada DAE FILE: " << filename << std::endl);

            // scale model if not in meters already
            if ((options != NULL) && (options->getPluginData("DAE-AssetUnitMeter") != NULL)) {
                float unitMeter = *(float*)(options->getPluginData("DAE-AssetUnitMeter"));
                if ((unitMeter != 1.0) && (unitMeter != 0.0)) {
                    node = ScaleGraph(node, unitMeter);
                }
            }

            // optimize model and save cached model to fast loading "ive" 
            ret = OptimizeAndSaveModel(node, cacheFilename);

        }
    }

    return ret;

}
